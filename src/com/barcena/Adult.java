package com.barcena;

public class Adult extends Human{

    // Create a property occupation
    private String occupation;

    // Create an empty constructor
    public Adult() {}

    // Parameterized constuctor
    public Adult(String name, int age, char gender, String newOccupation) {
        super(name, age, gender);
        this.occupation = newOccupation;
    }

    // Create the setter and getter methods for occupation
    public String getOccupation(){
        return this.occupation;
    }

    public void setOccupation(String newOccupation){
        this.occupation = newOccupation;
    }
}
