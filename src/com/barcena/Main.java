package com.barcena;

public class Main {

    public static void main(String[] args) {
	// write your code here
        // Mini-activity:
        // Create a new Human class with the following properties and methods:
        // Properties:
        // name, age, gender (single character)
        // Methods:
        // talk --> display a message to the console "Hello, my name is " concatenated to the user's name
        // setters and getters methods for the properties
        // Constructor: implement both empty and parameterized

        Adult adult1 = new Adult("Juan", 35, 'M', "Lawyer");
        System.out.println(adult1.talk());
        System.out.println(adult1.getOccupation());

        // Create a new instance of the Child class
        // Invoke the talk method afterwards
        Child child1 = new Child("John", 15, 'M', "Computer Science Major");
        System.out.println(child1.talk());
        System.out.println(child1.getEducation());
    }
}
