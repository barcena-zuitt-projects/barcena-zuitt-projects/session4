package com.barcena;

public class Human {

    // Properties
    private String name;
    private int age;
    private char gender ;

    // Constructors
    public Human() {}

    public Human(String newName, int newAge, char newGender) {
        System.out.println("Data addded");
        this.name = newName;
        this.age = newAge;
        this.gender = newGender;

    }

    // Getters
    public String getName() { return this.name; }

    public int getAge() { return this.age; }

    public char getGender() { return this.gender; }

    // Setters
    public void setName(String newName) { this.name = newName; }

    public void setAge(int newAge) { this.age = newAge; }

    public void setGender(char newGender) { this.gender = newGender; }

    // Method
    /*public void display() {
        System.out.println("Hello, my name is " + name);
    }*/
    public String talk() {
        String message = "Hello my name is " + getName() + ".";
        return message;
    }
}
