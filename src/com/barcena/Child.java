package com.barcena;

public class Child extends Human {

    // Create a property education
    private String education;

    // create an empty constructor
    public Child() {}

    // Parameterized constructor
    public Child(String name, int age, char gender, String newEducation) {
        super(name, age, gender);
        this.education = newEducation;
    }

    // Create the setter and getter methods for education
    public String getEducation() {
        return this.education;
    }

    public void setEducation(String newEducation) {
        this.education = newEducation;
    }
}
